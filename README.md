# Jobsity Python Challenge

This project contains 4 different things
1) A Python3.8 Flask API which connects to a MySQL database. 
2) A simple Angular UI to show how the API works.
3) A Python bot, decoupled from the API as requested, in order to interact with said API.
4) A series of Docker files in order to simplify multi platform validation of this project.

I chose to put everything in a single repository in order to simplify cloning and running. 

## Requirements
The only thing you'll need is Docker.

If you're using MacOS or Windows, you can download a desktop client [here](https://hub.docker.com/?overlay=onboarding).

For Linux distros, you can find instructions [here](https://docs.docker.com/install/linux/docker-ce/debian/).  

## Installation

`git clone git@gitlab.com:Fxwfran/jobsity-challenge.git`

`cd jobsity-challenge` 

`docker-compose up`


## Usage
During `docker-compose`, wait until you see the following log in your terminal:

`challenge_ui_1   | ℹ ｢wdm｣: Compiled successfully.`

Then open your favorite browser at http://localhost:4200/

### Notes
It might take about 5 minutes to build for the first time, after that it'll take about 15 seconds to run.

This also depends on your connection, so I've used Alpine-based Docker images in order to make it a light experience.

Since all images (Except MySql) are Alpine-based, the first layer of most containers will only download once. I hope this helps as well.