import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatInputModule} from "@angular/material/input";
import {MatIconModule} from "@angular/material/icon";
import {MatFormFieldModule} from "@angular/material/form-field";
import {ChatsComponent} from './views/chats/chats.component';
import {LoginComponent} from './views/login/login.component';
import {AuthGuard} from "./auth.guard";
import {MatButtonModule} from "@angular/material/button";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatAutocompleteModule} from "@angular/material/autocomplete";
import { SocketIoModule } from 'ngx-socket-io';
import {environment} from "../environments/environment";

@NgModule({
  declarations: [
    AppComponent,
    ChatsComponent,
    LoginComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatIconModule,
    MatFormFieldModule,
    MatButtonModule,
    FormsModule,
    MatSnackBarModule,
    MatAutocompleteModule,
    SocketIoModule.forRoot({ url: environment.apiUrl, options: {} })
  ],
  providers: [AuthGuard, HttpClientModule, MatSnackBarModule],
  bootstrap: [AppComponent]
})
export class AppModule {
}
