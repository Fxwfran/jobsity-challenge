import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ChatsComponent} from "./views/chats/chats.component";
import {LoginComponent} from "./views/login/login.component";
import {AuthGuard} from "./auth.guard";


const routes: Routes = [
    {path: 'chats', component: ChatsComponent, canActivate: [AuthGuard]},
    {path: 'login', component: LoginComponent},
    { path: '**', redirectTo: '/chats' }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
