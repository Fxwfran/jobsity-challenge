import {AfterViewChecked, Component} from '@angular/core';
import {AuthService} from "../../services/auth.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements AfterViewChecked {
  email: string;
  password: string;
  email2: string;
  password2: string;
  username: string;

  constructor(private _router: Router, private _authService: AuthService) {
    this.email = "";
    this.password = "";
    this.email2 = "";
    this.password2 = "";
    this.username = "";
  }

  ngAfterViewChecked() {
    if (this._authService.isAuthenticated()) this._router.navigate(['/chats']);
  }

  login() {
    this._authService.login(this.email, this.password)
  }

  signup() {
    this._authService.signup(this.email2, this.password2, this.username)
  }
}
