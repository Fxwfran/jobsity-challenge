import {AfterViewChecked, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Router} from "@angular/router";
import {MessagingService, OpenChat, Preview} from "../../services/messaging.service";
import {MatSnackBar} from "@angular/material/snack-bar";


@Component({
  selector: 'app-chats',
  templateUrl: './chats.component.html',
  styleUrls: ['./chats.component.scss']
})
export class ChatsComponent implements OnInit, AfterViewChecked {
  userSearch: string;
  options: any[];
  openChat: OpenChat;
  sidebarChats: Preview[];
  inputMessage: string;
  @ViewChild('scroller', {static: true}) private chatElmRef: ElementRef;
  private subscription: any;

  constructor(private _router: Router,
              private messagingService: MessagingService,
              private _snackBar: MatSnackBar) {
    this.userSearch = "";
    this.openChat = {
      messages: [],
      username: null,
      id: null
    };
  }

  ngOnInit() {
    this.loadMainChat(this.openChat.id);
    this.messagingService.socket.on('newMsg', (payload) => {
      this.loadSidebar();
      const commands = ['stock'];
      for (const command of commands) {
        if (payload["content"].includes(`/${command}=`)) return;
      }
      if (payload["senderUsername"] === "Jobsity Bot") return;
      if ([payload["from"], payload["to"]].includes(this.openChat.id)) {
        this.openChat.messages.push({
          at: new Date().toUTCString(),
          from: payload["from"],
          text: payload["content"],
          to: payload["to"],
          senderUsername: payload["senderUsername"],
          isFromBot: false
        })
      }
    });
    this.messagingService.socket.on('newBotMsg', (payload) => {
      this.loadSidebar();
      this.openChat.messages.push({
        at: new Date().toUTCString(),
        from: payload["from"],
        text: payload["content"],
        to: payload["to"],
        senderUsername: payload["senderUsername"],
        isFromBot: true
      });
    });
    this.loadSidebar();
    this.scrollChat();
  }

  ngAfterViewChecked() {
    this.scrollChat();
  }

  onSearchInputChanged(value: any) {
    this.options = [];
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    this.subscription = this.messagingService.searchUsers(value).subscribe(
      (result) => {
        this.options = result;
      },
      (error) => {
        this.messagingService.handleException(error);
      });
  }

  scrollChat() {
    this.chatElmRef.nativeElement.scrollTop = this.chatElmRef.nativeElement.scrollHeight;
  }

  selectedChat(option: any) {
    this.openChat = option;
    this.loadMainChat(option.id)
  }

  sendMessage() {
    this.messagingService.sendMessage(this.openChat.id, this.inputMessage).subscribe(
      (result) => {
        this.messagingService.socket.emit('sendMsg', {
          from: this.messagingService.accessToken,
          to: this.openChat.id,
          message_body: this.inputMessage
        });
        this.inputMessage = "";
        this.loadSidebar();
        this.scrollChat();
      },
      (error) => {
        this.messagingService.handleException(error);
      }
    );
  }

  logout() {
    localStorage.removeItem('accessToken');
    this._router.navigate(['/login']);
  }

  private loadSidebar() {
    this.messagingService.loadChatPreviews().subscribe(
      (result) => {
        this.sidebarChats = result.sort((a: any, b: any) => {
          let left = Number(new Date(a.time));
          let right = Number(new Date(b.time));
          return right - left;
        }).map((chat) => {
          return {
            ...chat,
            last_message: chat.last_message.slice(0, 32) + (chat.last_message.length > 31 ? '...' : ''),
          }
        });
      },
      (error) => {
        this.messagingService.handleException(error);
      });

  }

  private loadMainChat(userId) {
    this.messagingService.openChat(userId).subscribe(
      (result) => {
        this.openChat.messages = result.sort((a: any, b: any) => {
          let left = Number(new Date(a.at));
          let right = Number(new Date(b.at));
          return left - right;
        });
        this.scrollChat();
      },
      (error) => {
        this.messagingService.handleException(error);
      }
    );
  }
}
