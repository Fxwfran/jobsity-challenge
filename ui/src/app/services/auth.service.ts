import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private apiUrl: string;
  private headers: { "Content-Type": string };

  constructor(private http: HttpClient, private _snackBar: MatSnackBar, private _router: Router) {
    this.apiUrl = environment.apiUrl;
    this.headers = {"Content-Type": "application/json"}
  }

  isAuthenticated() {
    return !!localStorage.getItem('accessToken');
  }

  login(email, password) {
    this.http.post(this.apiUrl + "/auth/login", {email, password}, {headers: this.headers})
        .subscribe(
          (response) => {
            this._snackBar.open("Welcome!", 'x', {duration: 2000});
            localStorage.setItem('accessToken', response["accessToken"]);
            this._router.navigate(['/chats']);
          },
          (exception) => {
            this.handleException(exception)
          }
        )
  }

  signup(email, password, username) {
    this.http.post(this.apiUrl + "/auth/signup", {email, password, username}, {headers: this.headers})
        .subscribe(
          (response) => {
            this.login(email, password);
          },
          (exception) => {
            this.handleException(exception)
          }
        )
  }

  private handleException(exception) {
    let errorMessage = exception.error.message
      ? exception.error.message
      : "We're sorry, it seems you have lost connection.";
    this._snackBar.open(errorMessage, 'x', {duration: 2000});
  }
}
