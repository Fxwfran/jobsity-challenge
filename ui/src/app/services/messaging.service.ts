import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";
import {Socket} from "ngx-socket-io";

export interface Preview {
  last_message: string,
  time: Date,
  with: {
    id: number,
    username: string,
  }
}

export interface OpenChat {
  id: number,
  username: string,
  messages: Message[],
}


export interface Message {
  at: string,
  from: number,
  text: string,
  to: number,
  senderUsername?: string,
  isFromBot?: boolean
}

@Injectable({
  providedIn: 'root'
})
export class MessagingService {
  private apiUrl: string;

  constructor(private http: HttpClient, private _router: Router, private _snackBar: MatSnackBar, public socket: Socket) {
    this.apiUrl = environment.apiUrl;
    this.socket.emit('init', this.accessToken);
  }

  get accessToken() {
    return localStorage.getItem('accessToken');
  }

  get requestHeaders(): any {
    return {"Content-Type": "application/json", Authorization: this.accessToken}
  }

  searchUsers(searchTerm = ""): Observable<Object[]> {
    return <Observable<Object[]>>this.http.get(this.apiUrl + `/users?search_term=${searchTerm}`, {headers: this.requestHeaders});
  }

  openChat(otherUserId: number): Observable<Message[]> {
    return <Observable<Message[]>>this.http.get(
      this.apiUrl + '/chat/' + otherUserId,
      {headers: this.requestHeaders}
    );
  }

  loadChatPreviews() {
    return <Observable<Preview[]>>this.http.get(
      this.apiUrl + '/chat',
      {headers: this.requestHeaders}
    );
  }

  sendMessage(to, messageBody) {
    return <Observable<Object[]>>this.http.post(
      this.apiUrl + '/chat/message',
      {to: to, message_body: messageBody},
      {headers: this.requestHeaders}
    );
  }

  handleException(exception) {
    let errorMessage = exception.error.message
      ? exception.error.message
      : "We're sorry, it seems you have lost connection.";
    this._snackBar.open(errorMessage, 'x', {duration: 2000});
  }
}
