# Libs
import pika

# Modules
from rabbit.queues import stock

# Init
connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq', port=5672))
channel = connection.channel()

# Queues declaration
stock.declared_for(channel)

print(' [*] RabbitBot successfully initialized. Waiting for messages.')
channel.start_consuming()
