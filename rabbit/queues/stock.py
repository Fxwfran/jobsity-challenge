# Native libs
import csv
import json
import os

# Pip libs
import requests
import socketio

# Modules
from rabbit.helpers.authentication import get_access_token


def declared_for(channel):
    channel.queue_declare(queue='stock')

    def callback(ch, method, properties, body):
        print(' [*] RabbitBot - Incomming connection.')
        payload = json.loads(body)
        url = 'https://stooq.com/q/l/?s={}&f=sd2t2ohlcv&h&e=csv'.format(payload["parameter"])
        response = requests.get(url)
        messages = []
        with open('temp.csv', 'wb') as csv_file:
            csv_file.write(response.content)
        with open('temp.csv', 'r') as csv_file:
            headers = ["Symbol", "Date", "Time", "Open", "High", "Low", "Close", "Volume"]
            csv_reader = csv.DictReader(csv_file, headers)
            for row in csv_reader:
                if row["Symbol"] == "Symbol":
                    continue
                messages.append("{} quote is ${} per share".format(row["Symbol"], row["Close"]))
        os.remove('temp.csv')
        for msg in messages:
            print(' [*] RabbitBot - Message processed.')
            sio = socketio.Client()
            sio.connect('http://challenge_api:5000')
            sio.emit(
                'sendBotMsg',
                {
                    "from": payload["from_user"],
                    "to": payload["to_user"],
                    "senderUsername": "Jobsity Bot",
                    "message_body": msg
                }
            )
            print(' [*] RabbitBot - SocketIO event emitted.')

    channel.basic_consume(queue='stock', auto_ack=True, on_message_callback=callback)
