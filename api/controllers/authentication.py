# Pip libs
from flask import request
from werkzeug.security import generate_password_hash, check_password_hash

# Modules
from api.helpers import http
from api.helpers.auth import decode_access_token, generate_access_token
from api.services.database import db, User


def inject_into(api):
    @api.before_request
    def before_req():
        public_endpoints = ['login', 'signup']
        authorized = request.endpoint in public_endpoints or request.method == 'OPTIONS'
        jwt_token = request.headers.get('Authorization')
        if not authorized and not decode_access_token(jwt_token):
            return http.response(401, {"message": "You're not authorized to perform this action."})

    @api.route('/auth/signup', methods=['POST'])
    def signup():
        body = request.json
        try:
            existing_user = User.query.filter_by(email=body["email"]).first()
            if existing_user:
                return http.response(400, {"message": "That e-mail is already registered"})
            hashed_password = generate_password_hash(body["password"])
            new_user = User(username=body["username"], email=body["email"], password=hashed_password)
            db.session.add(new_user)
            db.session.commit()
            return http.response(202, {"message": "Registered successfully as %s" % new_user.username})
        except (KeyError, TypeError):
            return http.response(400, {"message": "Required fields not sent"})

    @api.route('/auth/login', methods=['POST'])
    def login():
        body = request.json
        try:
            user = User.query.filter_by(email=body["email"]).first()
            if not user or not check_password_hash(user.password, body["password"]):
                return http.response(401, {"message": "Invalid credentials"})
            else:
                token = generate_access_token(user)
                return http.response(200, {"accessToken": token.decode("utf-8")})
        except (KeyError, TypeError):
            return http.response(400, {"message": "Required fields not sent"})
