from flask import request
from flask_socketio import SocketIO, emit

from api.helpers.auth import decode_access_token
from api.services.database import db, User


def inject_into(api):
    socketio = SocketIO(api, logger=False, engineio_logger=False, cors_allowed_origins="*")

    @socketio.on('init')
    def on_init(access_token):
        print(access_token)
        payload = decode_access_token(access_token)
        if payload:
            user = User.query.filter_by(id=payload["sub"]).first()
            user.socket_id = request.sid
            db.session.add(user)
            db.session.commit()

    @socketio.on('sendMsg')
    def on_message_sent(message):
        payload = decode_access_token(message["from"])
        user_from = User.query.filter_by(id=int(payload["sub"])).first()
        user_to = User.query.filter_by(id=message["to"]).first()
        payload = {
                    "from": user_from.id,
                    "to": user_to.id,
                    "senderUsername": payload["username"],
                    "content": message["message_body"]
                }
        emit('newMsg', payload, room=user_from.socket_id)
        emit('newMsg', payload, room=user_to.socket_id)

    @socketio.on('sendBotMsg')
    def on_bot_message(message):
        user_from = User.query.filter_by(id=message["from"]).first()
        user_to = User.query.filter_by(id=message["to"]).first()
        payload = {
            "from": user_from.id,
            "to": user_to.id,
            "senderUsername": "Jobsity Bot",
            "content": message["message_body"]
        }
        emit('newBotMsg', payload, room=user_from.socket_id)
