# Pip libs
from flask import request

# Modules
from api.helpers import http
from api.services.database import User


def inject_into(api):
    @api.route('/users', methods=['GET'])
    def search_users():
        search_term = request.args.get('search_term') or ""
        users_list = User.query.filter(User.username.contains(search_term)).all()
        result = [
            {
                "id": user.id,
                "username": user.username
            }
            for user in users_list
        ]
        return http.response(200, result)
