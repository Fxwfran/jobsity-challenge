# Built-in libs
from datetime import datetime

# Pip libs
from flask import request

# Modules
from api.helpers import http
from api.helpers.auth import decode_access_token
from api.services import rabbitmq
from api.services.database import Message, db, Chat


def inject_into(api):
    @api.route('/chat', methods=['GET'])
    def get_chats():
        jwt_token = request.headers.get('Authorization')
        user_id = decode_access_token(jwt_token)["sub"]
        chats = Chat.query.filter((Chat.user1_id == user_id) | (Chat.user2_id == user_id)).all()
        result = []
        for chat in chats:
            other_user = chat.user1 if chat.user2_id == int(user_id) else chat.user2
            result.append(
                {
                    "with": {"id": other_user.id, "username": other_user.username},
                    "last_message": chat.last_message.content,
                    "time": chat.last_message.sent_date
                }
            )
        return http.response(200, result)

    @api.route('/chat/<other_user_id>', methods=['GET'])
    def get_messages(other_user_id):
        jwt_token = request.headers.get('Authorization')
        user_id = decode_access_token(jwt_token)["sub"]
        messages = Message.query.filter(
            (
                    (Message.from_user == user_id) & (Message.to_user == other_user_id) |
                    (Message.from_user == other_user_id) & (Message.to_user == user_id)
            )
        ).order_by(Message.id.desc()).limit(50)
        result = [
            {
                "from": message.from_user,
                "to": message.to_user,
                "at": message.sent_date,
                "text": message.content
            }
            for message in messages
        ]
        return http.response(200, result)

    @api.route('/chat/message', methods=['POST'])
    def send_message():
        body = request.json
        try:
            jwt_token = request.headers.get('Authorization')
            user_id = decode_access_token(jwt_token)["sub"]
            to_user = body["to"]
            message_body = body["message_body"]
            rabbit_command = rabbitmq.received_command(message_body)
            if rabbit_command:
                payload = {
                    "queue": rabbit_command["queue"],
                    "parameter": rabbit_command["parameter"],
                    "from_user": user_id,
                    "to_user": to_user
                }
                rabbitmq.send_message(payload)
                return http.response(202, {"message": "Message sent successfully!"})
            new_message = Message(from_user=user_id,
                                  to_user=to_user,
                                  sent_date=datetime.utcnow(),
                                  content=message_body)
            db.session.add(new_message)
            db.session.commit()
            chat = Chat.query.filter(
                (
                        (Chat.user1_id == user_id) & (Chat.user2_id == body["to"]) |
                        (Chat.user1_id == body["to"]) & (Chat.user2_id == user_id)
                )
            ).first()
            if chat:
                chat.last_message_id = new_message.id
            else:
                chat = Chat(user1_id=user_id, user2_id=to_user, last_message_id=new_message.id)
            db.session.add(chat)
            db.session.commit()
            return http.response(202, {"message": "Message sent successfully!"})
        except (KeyError, TypeError):
            return http.response(400, {"message": "Required fields not sent"})
