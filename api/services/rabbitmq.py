# Native libs
import json

# Pip libs
import pika
from werkzeug.security import generate_password_hash

# Modules
from api.services.database import User

# Constants
available_commands = ["stock"]


def generate_bot_user(db):
    installed_already = User.query.filter_by(username='Jobsity Bot').first()
    if not installed_already:
        bot = User(username='Jobsity Bot', email='botman@botmail.com', password=generate_password_hash('Test1234!'))
        db.session.add(bot)
        db.session.commit()


def received_command(message_body):
    try:
        for command in available_commands:
            if message_body.startswith("/{}=".format(command)):
                [queue, parameter] = message_body.split("=")
                return {"queue": command, "parameter": parameter}
        return False
    except ValueError:
        return False


def send_message(payload):
    connection = pika.BlockingConnection(pika.ConnectionParameters(host='rabbitmq', port=5672))
    channel = connection.channel()
    json_payload = json.dumps(payload)
    channel.queue_declare(queue=payload["queue"])
    channel.basic_publish(exchange='',
                          routing_key=payload["queue"],
                          body=json_payload)
    print(" [x] Sent '{}' to '{}' queue".format(json_payload, payload["queue"]))
    connection.close()
