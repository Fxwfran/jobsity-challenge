from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm import backref, relationship

db = SQLAlchemy()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    password = db.Column(db.String(120), nullable=False)
    socket_id = db.Column(db.String(255), nullable=True)

    def __repr__(self):
        return '<User {}>'.format(self.username)


class Message(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    from_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    to_user = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    sent_date = db.Column(db.DateTime, nullable=False)
    content = db.Column(db.String(255), nullable=False)

    def __repr__(self):
        return '<Message {}>'.format(self.id)


class Chat(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user1_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user1 = relationship("User", backref=backref("user1", uselist=False), foreign_keys=[user1_id])
    user2_id = db.Column(db.Integer, db.ForeignKey('user.id'), nullable=False)
    user2 = relationship("User", backref=backref("user2", uselist=False), foreign_keys=[user2_id])
    last_message_id = db.Column(db.Integer, db.ForeignKey('message.id', onupdate="CASCADE"), nullable=False)
    last_message = relationship("Message", backref=backref("message", uselist=False))

    def __repr__(self):
        return '<Chat {}>'.format(self.id)
