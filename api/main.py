# Built-in libs
import logging
from os import environ

# Pip libs
from flask import Flask
from flask_cors import CORS

# Modules
from api.controllers import authentication, messaging, users, socket
from api.services.database import db
from api.services.rabbitmq import generate_bot_user

# Init
api = Flask(__name__)
CORS(api)
api.config['SQLALCHEMY_DATABASE_URI'] = environ['SQLALCHEMY_DATABASE_URI']
api.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
api.config['SECRET_KEY'] = environ['JWT_SECRET_KEY']
logging.getLogger('socketio').setLevel(logging.ERROR)
logging.getLogger('engineio').setLevel(logging.ERROR)
db.init_app(api)
api.app_context().push()
db.create_all()
generate_bot_user(db)

# Controllers
authentication.inject_into(api)
socket.inject_into(api)
messaging.inject_into(api)
users.inject_into(api)

if __name__ == '__main__':
    api.run(host='0.0.0.0', debug=True)
