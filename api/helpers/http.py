from flask import make_response, jsonify


def response(status_code, body):
    headers = {"Content-Type": "application/json"}
    return make_response((jsonify(body), status_code, headers))
