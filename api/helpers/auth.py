# Built-in libs
import time

# Pip libs
from jwt import decode, DecodeError, encode


def decode_access_token(token):
    try:
        payload = decode(token, "someKey")
        return payload
    except DecodeError:
        return False


def generate_access_token(user):
    jwt_token = encode({
        "sub": str(user.id),
        "username": user.username,
        "iat":  int(time.time())
    }, "someKey")
    return jwt_token
